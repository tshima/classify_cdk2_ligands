
import os
import sys
import math
import numpy
from openbabel import pybel


def get_li_atoms_from_string_xyz(in_xyz_str):
    lines   = in_xyz_str.strip().split("\n")
    N_lines = len(lines)
    words   = lines[0].split()
    N_atoms = int(words[0])
    li_atom = []
    cc      = 0
    for i in range(2, N_lines):
        words = lines[i].split()
        tmp_a = (words[0], float(words[1]), float(words[2]), float(words[3]), cc)
        cc   += 1
        li_atom.append(tmp_a)
    return li_atom
    
def get_li_atoms_from_string_pdbqt(in_str):
    omol    = pybel.readstring("pdbqt", in_str)
    omol.addh()
    xyz_str = omol.write("xyz")
    return get_li_atoms_from_string_xyz(xyz_str)

def cal_dis(atm1, atm2):
    x1 = atm1[1]
    y1 = atm1[2]
    z1 = atm1[3]
    x2 = atm2[1]
    y2 = atm2[2]
    z2 = atm2[3]
    r2 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2)
    r  = math.sqrt(r2)
    return r



def get_res_from_receptor(fname_receptor):
    fi = open(fname_receptor)
    lines = fi.readlines()
    fi.close()
    pre_res_no = -12345
    li_res = []
    for line in lines:
        words = line.split()
        if words[0]=="ATOM":
            res_num   = int(words[5])
            res_name  = words[3]
            atom_no   = int(words[1])
            x         = float(words[6])
            y         = float(words[7])
            z         = float(words[8])
            atom_kind = words[11].upper()
            if res_num!=pre_res_no:
                li_res.append([])
                pre_res_no = res_num
            tmp_atom  = (atom_kind, x, y, z, res_name, res_num, atom_no)
            li_res[-1].append(tmp_atom)
    return li_res


def cal_SC_score(li_atm1, li_atm2, para_gauss_exp=0.1, para_factor=1.0):
    N_li_atm1 = len(li_atm1)
    N_li_atm2 = len(li_atm2)
    ret_score = 0.0
    for i in range(N_li_atm1):
        atm1  = li_atm1[i]
        kind1 = li_atm1[i][0]
        if kind1=="O" or kind1=="N" or kind1=="S" or kind1=="F": 
            pass
        else:
            continue
        for j in range(N_li_atm2):
            atm2  = li_atm2[j]
            kind2 = li_atm2[j][0]
            if kind1=="O" or kind1=="N" or kind1=="S" or kind1=="F": 
                pass
            else:
                continue
            r     = cal_dis(atm1, atm2)
            r2    = r*r
            score = para_factor*math.exp(-para_gauss_exp*r2)/r
            ret_score += score
    return ret_score





def cal_LJ_score(li_atm1, li_atm2):
    N_li_atm1 = len(li_atm1)
    N_li_atm2 = len(li_atm2)
    ret_score = 0.0
    for i in range(N_li_atm1):
        atm1  = li_atm1[i]
        kind1 = li_atm1[i][0]
        if kind1 == "H": continue
        r0_1  = 2.0
        e1    = 1.0
        for j in range(N_li_atm2):
            atm2  = li_atm2[j]
            kind2 = li_atm2[j][0]
            if kind2 == "H": continue
            r0_2  = 2.0
            e2    = 1.0
            r0_12 = 0.5*(r0_1+r0_2)
            e12   = math.sqrt(e1*e2)
            r     = cal_dis(atm1, atm2)
            s     = r0_12/r
            s2    = s*s
            s4    = s2*s2
            s6    = s4*s2
            s12   = s6*s6
            score = (e12*s12-2.0*e12*s6)
            ret_score += score
    ret_score = ret_score
    return ret_score





def get_no_for_res(li_res, name_res, no_res):
    N_li_res = len(li_res)
    for i in range(N_li_res):
        tmp_res = li_res[i][0]
        tmp_res_name = tmp_res[4]
        tmp_res_no   = int(tmp_res[5])
        if name_res==tmp_res_name and no_res==tmp_res_no:
            return i

def get_h_bond_info_lev(mol_no, fname_base, fname_receptor):
    max_loop   = 10    
    li_info = []
    select_info = 0
    for i in range(1, max_loop):
        fname = fname_base+str(mol_no)+"_"+str(i)+".pdbqt"
        if os.path.exists(fname):
            tmp_h_bond_info = analyze_h_bond(fname, fname_receptor, i)
            li_info.append( tmp_h_bond_info )
            if check_num_lev( tmp_h_bond_info ):
                select_info = i-1
                break 
    return li_info[select_info] 




def get_li_atoms_ligand(fname_ligand):
    #print "===== ligand ====="
    fi      = open(fname_ligand)
    istr    = fi.read()
    lines_l = istr.split("\n")
    words_l = lines_l[0].split()
    affinity = float(words_l[3])
    fi.close()
    li_atoms_ligand = get_li_atoms_from_string_pdbqt(istr)
    #for a in li_a_ligand:
    #    print a
    return li_atoms_ligand, affinity

def get_target_no_from_hscore_li_leu83(mol_no, fname_base, li_leu83, th_affinity=-6.5):
    max_loop      = 10    
    max_SC_score   = 0
    ret_target_no = 1
    ret_li_atoms  = None
    ret_affinity   = None
    for i in range(1, max_loop):
        fname = fname_base+str(mol_no)+"_out_ligand_"+str(i)+".pdbqt"
        if os.path.exists(fname):
             li_atoms_ligand, affinity = get_li_atoms_ligand(fname)
             SC_score         = cal_SC_score(li_atoms_ligand, li_leu83)
             if i==1:
                 ret_li_atoms  = li_atoms_ligand
                 ret_affinity   = affinity
             if SC_score>max_SC_score and affinity<th_affinity:
                 ret_target_no = i 
                 ret_li_atoms  = li_atoms_ligand
                 ret_affinity   = affinity
                 max_SC_score   = SC_score
                 #print " ////  DEBUG ret_target_no ", ret_target_no, " SC_score ", SC_score, " affinity ", affinity
    ret_tup = (ret_target_no, ret_li_atoms, ret_affinity)
    return ret_tup


def get_target_no_from_hscore_li(mol_no, fname_base, th_affinity=-6.5):
    max_loop      = 10    
    max_SC_score   = 0
    ret_target_no = 1
    ret_li_atoms  = None
    ret_affinity   = None
    fname = fname_base+str(mol_no)+"_out_ligand_"+str(ret_target_no)+".pdbqt"
    li_atoms_ligand, affinity = get_li_atoms_ligand(fname)
    ret_li_atoms  = li_atoms_ligand
    ret_affinity   = affinity
    ret_tup = (ret_target_no, ret_li_atoms, ret_affinity)
    return ret_tup


def get_res_name(no, li_res):
    tmp_res  = li_res[no][0]
    tmp_name = tmp_res[4]
    tmp_no   = tmp_res[5]
    ret      = tmp_name+str(tmp_no)
    return ret

def run():
    a_or_d         = sys.argv[1]
    if a_or_d == "active":
        max_mol        = 798
    elif a_or_d == "decoy":
        max_mol        = 28328
    else:
        print("***** ERROR, please select 'active' or 'decoy'")
        quit()
    #fname_base     = "./actives_pdbqt/active_"
    fname_base     = "./"+a_or_d+"s_pdbqt/"+a_or_d+"_"

    fname_receptor_pdb = "res_cut4a.pdb"
    li_res         = get_res_from_receptor(fname_receptor_pdb)
    N_li_res       = len(li_res)
 
    for i in range(max_mol):
        mol_no     = i
        tmp_tup    = get_target_no_from_hscore_li(mol_no, fname_base)
        target_no  = tmp_tup[0]
        li_atoms_l = tmp_tup[1]
        affinity    = tmp_tup[2]
        li_LJ_hb_scores = []
        print("mol_no ", mol_no, " target_no ", target_no, " affinity ", affinity)
        for j in range(N_li_res):
            res_name = get_res_name(j, li_res)
            SC_score         = cal_SC_score(li_atoms_l, li_res[j])
            LJ_score       = cal_LJ_score(li_atoms_l, li_res[j])
            tup_j           = (res_name, SC_score, LJ_score) 
            li_LJ_hb_scores.append( tup_j )
            print("mol_no ", mol_no, " target_no ", target_no, " res_name ", res_name, " SC_score ", SC_score, " LJ_score ", LJ_score)
       

if __name__=="__main__":
   run()
