
import numpy
import pandas
import random
random.seed(100)

import sklearn.metrics as metrics
from sklearn.metrics import precision_recall_curve, precision_recall_fscore_support, average_precision_score
from sklearn.metrics import roc_curve
from sklearn.metrics import auc, roc_auc_score, classification_report
from sklearn import svm, preprocessing
from sklearn.model_selection import train_test_split
from sklearn.inspection import permutation_importance
import lightgbm
import shap
import pickle

import matplotlib.pyplot as plt

def read_afinity_data(fname):
    with open(fname) as fr:
        lines = fr.readlines()
    ret_lli = []
    pre_mol_no = -1
    for line in lines:
        words = line.split()
        mol_no = int(words[1])
        if pre_mol_no != mol_no: 
            #print("!=", words)
            if pre_mol_no!=-1: ret_lli.append(li_prop)
            afinity = float(words[5])
            li_prop = [afinity]
            li_column_name = ["affinity"] 
        if pre_mol_no == mol_no:
            #print("==", words) 
            res_name = words[5]
            li_column_name.append(res_name+"_SC")
            li_prop.append(float(words[7]))
            li_column_name.append(res_name+"_LJ")
            li_prop.append(float(words[9]))
        pre_mol_no = mol_no
    return li_column_name, ret_lli

def get_np_all_rand(lli_prop_actives, lli_prop_decoys):
    np_prop_actives = numpy.array(lli_prop_actives, dtype=numpy.double)
    np_prop_decoys  = numpy.array(lli_prop_decoys,  dtype=numpy.double)
    np_y_actives = numpy.ones(  (np_prop_actives.shape[0], 1), dtype=numpy.int)
    np_y_decoys  = numpy.zeros( (np_prop_decoys.shape[0], 1),  dtype=numpy.int)
    np_prop_all = numpy.concatenate([np_prop_actives, np_prop_decoys])
    np_y_all    = numpy.concatenate([np_y_actives, np_y_decoys])
    li_idx = list(range(np_y_all.shape[0]))
    random.shuffle(li_idx)
    np_prop_all_rand = np_prop_all[li_idx]
    np_y_all_rand    = np_y_all[li_idx]

    return li_idx, np_prop_all_rand, np_y_all_rand

def get_np_for_actives(lli_prop_actives):
    np_prop_actives = numpy.array(lli_prop_actives, dtype=numpy.double)
    np_y_actives = numpy.ones(  (np_prop_actives.shape[0], 1), dtype=numpy.int)
    return np_prop_actives, np_y_actives


def chunks2(lst, n_fold):
    ret = [[] for i in range(n_fold)]
    N_data = len(lst)
    for i in range(N_data):
        ret[i%n_fold].append(lst[i])
    return ret



def do_nfold(n_fold, li_features, li_targets, model, random_seed=1234):
    N_data = len(li_targets)
    li_num = list(range(N_data))
    if random_seed!=None:
        mod_seed = random_seed + 100
        random.seed(mod_seed)
        random.shuffle(li_num)
    li_chunks = chunks2(li_num, n_fold)
    NN = len(li_chunks)
    lli_pred        = []
    lli_proba       = []
    li_std          = []
    for i in range(NN):
        print("///// n_fold ", n_fold,",   i=", i+1)
        li_test_no = li_chunks[i]
        li_train_no = []
        for j in range(NN):
            if j==i: continue
            li_train_no += li_chunks[j]
        train_fv = []
        train_tg = []
        test_fv  = []
        test_tg  = []
        for no in li_test_no:
            test_fv.append(li_features[no])    
            test_tg.append(li_targets[no])    
        for no in li_train_no:
            train_fv.append(li_features[no])    
            train_tg.append(li_targets[no])   
        model.fit(train_fv, train_tg)
        np_pred_tg     = numpy.array(model.predict(test_fv))
        np_proba       = numpy.array(model.predict_proba(test_fv))[:,1]
        lli_pred.append(list(np_pred_tg))
        lli_proba.append(list(numpy.ravel(np_proba)))
    ret_pred  = [None for i in range(N_data)]
    ret_proba = [None for i in range(N_data)]
    for i in range(n_fold):
        for j in range(len(lli_pred[i])):
            ret_pred[li_chunks[i][j]] = lli_pred[i][j]
            ret_proba[li_chunks[i][j]] = lli_proba[i][j]
    matthews_corr = metrics.matthews_corrcoef(li_targets, ret_pred)
    acu_score     = metrics.accuracy_score(li_targets, ret_pred)
    fpr, tpr, _   = roc_curve(li_targets, ret_proba)
    value_auc     = auc(fpr, tpr)
    print("")
    print("   matthew_corr:   ", matthews_corr)
    print("   accuracy_score: ", acu_score)
    print("   value_auc       ", value_auc)
    return ret_pred, ret_proba




def factory_fun_cross_validation(num_nfold, li_features_scale, li_targets, random_state, get_model):
    def ret_fun(x):
        model = get_model(x, random_state)
        li_pred = do_nfold(num_nfold, li_features_scale, li_targets,  model, random_seed=random_state)
        matthews_corr = metrics.matthews_corrcoef(li_targets, li_pred)
        return -1.0*matthews_corr
    return ret_fun



def eval_model(num_nfold, np_train_fv_scale, np_targets, model):
    print("##### EVAL_MODEL #####")
    Y_pred, Y_proba = do_nfold(num_nfold, np_train_fv_scale, np_targets, model)
    prfs          = precision_recall_fscore_support(np_targets, Y_pred)
    print(classification_report(np_targets, Y_pred))
    matthews_corr = metrics.matthews_corrcoef(np_targets, Y_pred)
    ROC_AUC_score = roc_auc_score(np_targets, Y_proba)
    fpr, tpr, thres = roc_curve(np_targets, Y_proba)
    AUC_score = auc(fpr, tpr)
    print("   matthew_corr:   ", matthews_corr)
    print("   ROC_AUC socre:  ", ROC_AUC_score)
    print("   AUC socre:      ", AUC_score)
    return matthews_corr


def get_svm_model_rbf(x, random_state):
    ret_svm = svm.SVC(C=x, kernel="rbf", random_state=random_state, probability=True)
    return ret_svm


def get_lgb_model(x, random_state):
    ret_model = lightgbm.LGBMClassifier(num_leaves=x[0], max_depth=x[1], min_data_in_leaf=x[2], random_state=random_state)
    return ret_model

 
def factory_fun_score(in_model, in_cal_score):
    model = in_model
    cal_score = in_cal_score
    def fun_score(X, y):
        y_pred = model.predict(X)
        return cal_score(y, y_pred)
    return fun_score


def get_permutation_importances(ex_X, ex_Y, best_model, random_state, CAL_SCORE):
    r = permutation_importance(best_model, ex_X, ex_Y, n_repeats=30, random_state=random_state)
    ret = r.importances_mean
    return ret


def get_shap_importances_Tree(ex_X, ex_Y, best_model, random_state):
    shap.initjs()
    explainer = shap.TreeExplainer(model=best_model)
    shap_values = explainer.shap_values(ex_X)
    print(shap_values[0].shape)
    (Ni,Nj) = shap_values[0].shape
    shap_importances = numpy.zeros(Nj, dtype=numpy.double)
    for i in range(Ni):
        for j in range(Nj):
            shap_importances[j] +=abs(shap_values[0][i,j])
    shap_importances /= float(Ni)
    return shap_importances



def counterfactual_prediction(estimator, X, idx_to_replace, value_to_replace):
    X_replaced = numpy.copy(X)
    X_replaced[:, idx_to_replace] = value_to_replace
    y_pred = estimator.predict_proba(X_replaced)[:,1]
    return y_pred


def cal_cps_mean(estimator, in_X, idx, N=25):
    L=3.0
    li_x = [-L + (2.0*L/N)*i for i in range(N+1)]
    cps_mean = numpy.array([counterfactual_prediction(estimator, in_X, idx, x).mean() for x in li_x])
    return li_x, cps_mean


def run():
    li_column_name_actives, lli_prop_actives = read_afinity_data("actives_score.txt")
    li_column_name_decoys,  lli_prop_decoys  = read_afinity_data("decoys_score.txt")

    li_idx, np_prop_all_rand, np_y_all_rand = get_np_all_rand(lli_prop_actives, lli_prop_decoys)
    np_prop_actives, np_y_actives = get_np_for_actives(lli_prop_actives)

    # scale
    sscaler_fv     = preprocessing.StandardScaler()
    np_prop_all_rand_scale = sscaler_fv.fit_transform(np_prop_all_rand)
    np_prop_actives_scale  = sscaler_fv.transform(np_prop_actives)


    n_fold = 5
    print("##### Light GBM #####")
    #model_lgb = get_lgb_model( (136, 113, 94), 333)
    #eval_model(n_fold, np_prop_all_rand_scale, np_y_all_rand, model_lgb)

    model_lgb = get_lgb_model( (136, 113, 94), 333)
    model_lgb.fit(np_prop_all_rand_scale, np_y_all_rand)

    print("##### PD for Light GBM #####")
    print(li_column_name_actives)
    idx = li_column_name_actives.index("affinity")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.rcParams["font.size"]=14
    plt.xlabel("affinity", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_affinity_actives_only.png")
    plt.show()


    idx = li_column_name_actives.index("Val18_SC")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.xlabel("Val18_SC", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Val18_SC_actives_only.png")
    plt.show()


    idx = li_column_name_actives.index("Val18_LJ")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.xlabel("Val18_LJ", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Val18_LJ_actives_only.png")
    plt.show()


    idx = li_column_name_actives.index("Leu134_SC")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.xlabel("Leu134_SC", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Leu134_SC_actives_only.png")
    plt.show()

    idx = li_column_name_actives.index("Leu134_LJ")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.xlabel("Leu134_LJ", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Leu134_LJ_actives_only.png")
    plt.show()


    idx = li_column_name_actives.index("Lys33_SC")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.xlabel("Lys33_SC", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Lys33_SC_actives_only.png")
    plt.show()

    idx = li_column_name_actives.index("Lys33_LJ")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_actives_scale, idx)
    plt.xlabel("Lys33_LJ", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Lys33_LJ_actives_only.png")
    plt.show()


    idx = li_column_name_actives.index("affinity")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("affinity", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_affinity.png")
    plt.show()


    idx = li_column_name_actives.index("Val18_SC")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("Val18_SC", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Val18_SC.png")
    plt.show()


    idx = li_column_name_actives.index("Val18_LJ")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("Val18_LJ", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Val18_LJ.png")
    plt.show()


    idx = li_column_name_actives.index("Leu134_SC")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("Leu134_SC", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Leu134_SC.png")
    plt.show()

    idx = li_column_name_actives.index("Leu134_LJ")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("Leu134_LJ", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Leu134_LJ.png")
    plt.show()


    idx = li_column_name_actives.index("Lys33_SC")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("Lys33_SC", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Lys33_SC.png")
    plt.show()

    idx = li_column_name_actives.index("Lys33_LJ")
    li_x, np_cps = cal_cps_mean(model_lgb, np_prop_all_rand_scale, idx)
    plt.xlabel("Lys33_LJ", fontsize=20)
    plt.scatter(li_x, np_cps, color="black")
    plt.plot(li_x, np_cps, color="black", label="ave")
    plt.tight_layout()
    plt.savefig("PD_Lys33_LJ.png")
    plt.show()


if __name__=="__main__":
    run()


