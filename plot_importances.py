
import pickle
import matplotlib.pyplot as plt

def get_li(N_lim, li_sort):
    ret_li       = []
    ret_li_label = []
    max_v = li_sort[0][1]
    for i in range(N_lim):
        tmp_str = li_sort[i][0]
        ret_li_label.append(tmp_str)
        ret_li.append(li_sort[i][1]/max_v)
    ret_li.reverse()
    ret_li_label.reverse()
    return ret_li, ret_li_label

def run():
    
    with open("models_importances.dump", "rb") as fr:
        load_dic = pickle.load(fr)

    li_sorted_pi_lgb = load_dic["li_sorted_pi_lgb"]
    li_sorted_si_lgb = load_dic["li_sorted_si_lgb"]
    li_sorted_pi_svm = load_dic["li_sorted_pi_svm"]

    N_lim2 = len(li_sorted_pi_lgb)
    li_pi_lgb2, li_pi_lgb_label2 = get_li(N_lim2, li_sorted_pi_lgb)
    li_si_lgb2, li_si_lgb_label2 = get_li(N_lim2, li_sorted_si_lgb)
    li_pi_svm2, li_pi_svm_label2 = get_li(N_lim2, li_sorted_pi_svm)


    N = len(li_sorted_pi_lgb)
    N_lim = 15

    li_x = range(0, N_lim)
    li_pi_lgb, li_pi_lgb_label = get_li(N_lim, li_sorted_pi_lgb)
    li_si_lgb, li_si_lgb_label = get_li(N_lim, li_sorted_si_lgb)
    li_pi_svm, li_pi_svm_label = get_li(N_lim, li_sorted_pi_svm)

    plt.rcParams["font.size"] = 22
    fig = plt.figure()
    fig.set_figheight(20)
    fig.set_figwidth(15)
    plt.barh(li_x, li_pi_lgb, tick_label=li_pi_lgb_label)
    plt.savefig("pi_lgb.png")
    plt.show()

    fig = plt.figure()
    fig.set_figheight(20)
    fig.set_figwidth(15)
    plt.barh(li_x, li_si_lgb, tick_label=li_si_lgb_label)
    plt.savefig("si_lgb.png")
    plt.show()

    fig = plt.figure()
    fig.set_figheight(20)
    fig.set_figwidth(15)
    plt.barh(li_x, li_pi_svm, tick_label=li_pi_svm_label)
    plt.savefig("pi_svm.png")
    plt.show()



if __name__=="__main__":
    run()


