# Classify ligands for CDK2
## Requirements
```
                          Version
python                    3.8.8
numpy                     1.20.3          
pandas                    1.3.2         
openbabel                 3.1.1       
scikit-learn              0.24.2 
lightgbm                  3.2.1          
shap                      0.39.0       
matplotlib                3.4.2          
```

## Usage
### 1, calculation of simplified SC and LJ scores
  We can calculate simplified SC and LJ scores for active and decoy molecules.
Those molecular structures are obtained from the Vina docking simulation. 
The PDBQT structure files for active and decoys molecules can be found in the "actives_pdbqt" and "decoys_pdbqt" directories, respectively.
The "res_cut4a.pdb" file contains residues around the ligand-pocket of CDK2.
To calculate simplified SC and LJ scores for active and decoy molecules, the following commands can be used.


```

python cal_scores.py active > actives_score.txt

python cal_scores.py decoy  > decoys.score.txt

```


### 2, Machine-learning classification and explainable AI analysis
  From simplified SC and LJ scores, we can classify active ligands based on machine learning. 
In the following sample script,  the shuffled "actives_scores.txt" and "decoys_score.txt" data is used for machine-learning classification.
Next, explainable AI analysis is executed. 
The sample script for these operations can be used as follows,


```

python cal_ML.py > cal_ML.out

```


### 3, Plot 
  To plot the explainable AI analysis, we can use the following command.


```

python plot_importances.py

```


